;;; init.el -*- lexical-binding: t; -*-

;;; debug
(when init-file-debug
  (switch-to-buffer (messages-buffer)))

;;; vars
(setq user-emacs-directory (file-name-directory load-file-name)
      initial-major-mode #'fundamental-mode)

;;; modules
(setq init+core
      '(
        ;; +++
        util+
        pkg+
        kbd+
        rice+
        ;; +++
        ))

(setq init+glue
      '(
        ;; +++
        dirs+
        modes+
        ;; +++
        ))

(setq init+modules
      '(
        ;; +++
        asm+
        bibtex+
        build+
        ;; cl+
        ;; clojure+
        completion+
        containers+
        cpp+
        ;; crystal+
        dart+
        data+
        dbg+
        dired+
        editor+
        elisp+
        emacs+
        email+
        ess+
        evil+
        format+
        gif+
        git+
        ;; golang+
        helm+
        img+
        latex+
        lint+
        lsp+
        ;; lua+
        nav+
        ;; nim+
        nix+
        org+
        pdf+
        progrice+
        project+
        prose+
        python+
        ;; racket+
        rust+
        shell+
        snippets+
        templates+
        term+
        transient+
        vhdl+
        wm+
        ;; +++
        ))

(defun init+require (modules)
  (dolist (module modules)
    (require module)))

(defmacro init+turbo (&rest body)
  (declare (debug t))
  `(let ((gc-cons-threshold most-positive-fixnum)
         (inhibit-message (not init-file-debug)))
     ,@body))

(defun init+cdr nil
  (init+turbo
   (init+require init+glue)
   (init+require init+modules)
   (maybe+ #'benchmark-init/deactivate)))

;;; init
(add-to-list 'load-path (concat user-emacs-directory "macs+/"))
(init+turbo (init+require init+core))
(if (or noninteractive init-file-debug (daemonp))
    (init+cdr)
  (hook+ 'pre-command-hook
         #'init+cdr nil nil t))

(provide 'init)
;;; init.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars lexical unresolved)
;; flycheck-disabled-checkers: (emacs-lisp-checkdoc)
;; End:
