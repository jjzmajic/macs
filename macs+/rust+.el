;;; rust+.el -*- lexical-binding: t; -*-

(use-package rustic
  :gfhook ('rustic-mode-hook
           '(flycheck-mode
             lsp-deferred))
  :init (setq rustic-lsp-server 'rls
              rustic-lsp-setup-p t)
  (defalias 'rust-mode #'rustic-mode)
  :general (kbd+local :keymaps 'rustic-mode-map
             kbd+localleader #'rustic-cargo-check
             "f" #'rustic-rustfix
             "c" #'rustic-cargo-clippy))

(after+ 'compdef
  (compdef :modes #'rustic-mode
           :company '((company-lsp
                       company-files
                       company-dabbrev-code
                       company-capf))))

(after+ 'handle
  (handle #'rustic-mode
          :gotos #'lsp-find-definition
          :docs #'lsp-describe-thing-at-point
          :compilers #'rustic-compile
          :formatters #'rustic-format-buffer))

(provide 'rust+)
;;; rust+.el ends here
