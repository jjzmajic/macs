;;; term+.el -*- lexical-binding: t; -*-

(setq term+terminal "kitty")
(defun emacs+setup-eshell nil
  "Set up eshell given that the keymap is buffer local."
  (eshell-cmpl-initialize)
  (general-def
    :keymaps 'eshell-mode-map
    :package 'eshell
    [remap eshell-pcomplete] #'helm-esh-pcomplete
    "M-r" #'helm-eshell-history))

(defun term+personal-server nil
  (interactive)
  (let ((default-directory
          "/ssh:root@178.128.55.37"))
    (vterm-toggle-cd)))

;;; *nix systems
(use-package vterm-toggle
  :init
  (setq
   vterm-toggle-use-dedicated-buffer nil
   vterm-toggle-cd-auto-create-buffer nil
   vterm-toggle-fullscreen-p nil)
  :general
  (kbd+
    "'" '(vterm-toggle-cd :wk "vterm")
    "ov" '(vterm-toggle-cd :wk "vterm"))
  (kbd+local :keymaps 'vterm-mode-map
    "c" #'vterm-toggle-insert-cd
    "p" #'vterm-toggle-backward
    "n" #'vterm-toggle-forward))

(use-package terminal-here
  :init
  (when (executable-find term+terminal)
    (setq terminal-here-terminal-command
          `(,term+terminal)))
  :general
  (kbd+
    "ot" '(terminal-here-launch
           :wk "open terminal")
    "pv" '(projectile-run-vterm
           :wk "open vterm")
    "pt" '(terminal-here-project-launch
           :wk "project terminal")))

;;; dos backup
(use-package eshell
  :defer t
  :gfhook
  ('eshell-mode-hook
   #'emacs+setup-eshell)
  :init
  (kbd+ "oe" #'eshell
    "be" '(helm-eshell-prompts-all
           :wk "eshell buffers"
           :package helm)))
(use-package fish-completion
  :after ehsell
  :if (executable-find "fish")
  :init (setq fish-completion-fallback-on-bash-p t)
  :config (global-fish-completion-mode +1))
(use-package bash-completion
  :if (executable-find "bash")
  :ghook ('eshell-mode-hook
          #'bash-completion-setup
          nil nil t))

(provide 'term+)
;;; term+.el ends here
