;;; util+.el -*- lexical-binding: t; -*-

(defalias 'list+ #'add-to-list)
(defalias 'hook+ #'add-hook)
(defalias 'advice+ #'advice-add)
(defalias 'after+ #'with-eval-after-load)

(defun enlist+ (exp)
  (declare (pure t) (side-effect-free t))
  (if (listp exp) exp (list exp)))

(defmacro maybe+ (fun &rest args)
  `(when (fboundp ,fun)
     (apply ,fun ,args)))

(provide 'util+)
;;; util+.el ends here
