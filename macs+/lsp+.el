;;; lsp+.el -*- lexical-binding: t; -*-

(defun lsp+company nil
  (when (featurep 'company+)
    (push 'company-lsp company-backends)))

(use-package lsp-mode
  :commands (lsp-workspace-folders-add)
  :init (setq lsp-auto-configure nil)
  :config
  (hook+ #'lsp-mode-hook '(lsp-flycheck-enable lsp-enable-imenu))
  :general (kbd+ "tl" '(lsp-mode :wk "lsp")))

(use-package lsp-ui
  :init
  (setq lsp-ui-doc-enable nil
        lsp-ui-imenu-enable nil
        lsp-ui-peek-enable nil
        lsp-ui-flycheck-enable t
        lsp-ui-sideline-enable t)
  :ghook 'lsp-mode-hook)

(use-package company-lsp
  :after company+
  :gfhook ('lsp-mode-hook
           #'lsp+company))

(provide 'lsp+)
;;; lsp+.el ends here
