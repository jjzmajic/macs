;;; data+.el -*- lexical-binding: t; -*-

(use-package yaml-mode
  :gfhook
  '(highlight-indent-guides-mode
    flycheck-mode
    hl-todo-mode)
  :defer t)
(use-package yaml-imenu
  :after yaml-mode
  :config (yaml-imenu-enable))
(use-package json-mode :defer t)
(use-package csv-mode :defer t)
(use-package graphql-mode :defer t)

(provide 'data+)
;;; json+.el ends here
