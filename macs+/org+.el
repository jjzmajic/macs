;;; org+.el -*- lexical-binding: t; -*-

;;; vars
(setq org+gmail "uros.m.perisic"
      org+gmail-suffix "@gmail.com"
      org+gcal-dir (dirs+sync "org/gcal")
      org+agenda-files
      '(
        ;; +++
        "org/college.org"
        "org/agenda.org"
        ;; +++
        )
      org+babel-langs
      '((emacs-lisp . t)
        (python . t)
        (R . t)))

;;; packages
(use-package org
  :defer t
  :after which-key
  :straight org-plus-contrib
  :init
  (setq org-agenda-files (mapcar #'dirs+sync org+agenda-files)
        org-highlight-latex-and-related  '(latex entities)
        org-eldoc-breadcrumb-separator which-key-separator
        org-agenda-breadcrumbs-separator which-key-separator
        org-adapt-indentation nil)
  :gfhook
  ('org-mode-hook
   '(yas-minor-mode
     flycheck-mode
     flyspell-mode
     auto-fill-mode
     auto-save-mode))
  :config
  (after+ 'flycheck
    (list+ 'flycheck-textlint-plugin-alist
           '(org-mode . "@textlint/text")))
  (after+ 'org
    (list+ 'org-latex-classes
           '("cas-sc" "\\documentclass[11pt]{cas-sc}"
             ("\\section{%s}" . "\\section*{%s}")
             ("\\subsection{%s}" . "\\subsection*{%s}")
             ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
             ("\\paragraph{%s}" . "\\paragraph*{%s}")
             ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
    (list+ 'org-latex-classes
           '("cas-dc" "\\documentclass[11pt]{cas-dc}"
             ("\\section{%s}" . "\\section*{%s}")
             ("\\subsection{%s}" . "\\subsection*{%s}")
             ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
             ("\\paragraph{%s}" . "\\paragraph*{%s}")
             ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))
  :general
  (general-def
    :keymaps 'org-mode-map
    :states 'normal
    [return] #'org-open-at-point)
  (kbd+ "oa" '(org-agenda-list :wk "org agenda"))
  (kbd+local
    :keymaps 'org-mode-map
    "," #'org-ctrl-c-ctrl-c
    "*" #'org-ctrl-c-star
    "-" #'org-ctrl-c-minus
    "'" #'org-edit-special
    "l" #'org-shiftright
    "h" #'org-shiftleft
    "k" #'org-shiftup
    "j" #'org-shiftdown
    "r" #'org-refile
    "a" #'org-attach
    "e" '(:wk "export")
    "s" '(:wk "subtree")
    "t" '(:wk "toggle")
    "d" '(:wk "date")
    "i" '(:wk "insert"))
  (kbd+local
    :infix "e"
    :keymaps 'org-mode-map
    "p" #'org-latex-export-to-pdf
    "b" #'org-beamer-export-to-pdf
    "e" #'org-export-dispatch)
  (kbd+local
    :infix "s"
    :keymaps 'org-mode-map
    "l" #'org-demote-subtree
    "h" #'org-promote-subtree
    "k" #'org-move-subtree-up
    "j" #'org-move-subtree-down)
  (kbd+local
    :infix "t"
    :keymaps 'org-mode-map
    "b" #'org-beamer-mode
    "l" #'org-toggle-link-display
    "i" #'org-toggle-inline-images)
  (kbd+local
    :infix "d"
    :keymaps 'org-mode-map
    "d" #'org-deadline
    "s" #'org-schedule)
  (kbd+local
    :infix "i"
    :keymaps 'org-mode-map
    "c" #'org-ref-helm-insert-cite-link
    "f" #'org-footnote-new
    "t" #'org-export-insert-default-template
    "h" #'org-insert-heading
    "s" #'org-insert-subheading
    "i" #'org-screenshot-take))

(use-package ob
  :straight nil
  :after org
  :init (setq org-confirm-babel-evaluate nil)
  :config (org-babel-do-load-languages
           'org-babel-load-languages
           org+babel-langs))

(use-package evil-org
  :after evil+
  :hook (org-mode . evil-org-mode)
  ;; [[~/.emacs.d/straight/repos/evil-org-mode/doc/keythemes.org]]
  :init
  (setq evil-org-key-theme
        '(navigation
          textobjects
          additional
          calendar
          shift))
  :config
  (evil-org-set-key-theme)
  ;; patch [bactab] until fixed
  (general-def
    :keymaps 'org-mode-map
    :states '(normal visual)
    [backtab] #'org-shifttab))

(use-package evil-org-agenda
  :straight evil-org
  :after org-agenda
  :config (evil-org-agenda-set-keys))

(use-package org-ref
  :after org)
(use-package org-ref-helm
  :straight org-ref
  :after helm+
  :general
  (kbd+local
    :keymaps 'org-mode-map
    "ic" #'org-ref-helm-insert-cite-link
    "ir" #'org-ref-helm-insert-ref-link
    "il" #'org-ref-helm-insert-label-link))

(use-package org-gcal
  :init
  (setq org-gcal-client-id
        (concat "800850979629-5jiat6cpagi2l7fgrlh6eucsb31ki8ch."
                "apps.googleusercontent.com")
        org-gcal-client-secret "KvrrW1dBUF1U93ozXD9_Iz6W"
        org-gcal-file-alist
        `((,(concat org+gmail org+gmail-suffix)
           . ,(concat org+gcal-dir org+gmail ".org"))))
  :general (kbd+ "os" '(org-gcal-sync :wk "org sync")))

(use-package org-cliplink
  :after org
  :general (kbd+local
             :keymaps 'org-mode-map
             "il" #'org-cliplink))

(use-package org-superstar
  :ghook 'org-mode-hook)

(setq org-latex-pdf-process
      '("latexmk -g -pdf -pdflatex=\"%latex\" -outdir=%o %f"))

(after+ 'handle
  (handle #'org-mode
          :compilers #'org-latex-export-to-pdf))

(after+ 'compdef
  (compdef
   :modes #'org-mode
   :capf #'pcomplete-completions-at-point
   :company '((company-capf
               company-dabbrev)
              company-files
              company-bibtex)))

(provide 'org+)
;;; org+.el ends here
