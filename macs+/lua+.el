;;; lua+.el -*- lexical-binding: t; -*-

(use-package lua-mode
  :defer t
  :config
  (after+ 'handle+
    (handle #'lua-mode
            :repls #'lua-start-process
            :evaluators #'lua-send-buffer
            :evaluators-line #'lua-send-current-line)))

(provide 'lua+)
;;; lua+.el ends here
