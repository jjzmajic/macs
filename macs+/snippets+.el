;;; snippets+.el -*- lexical-binding: t; -*-

(use-package yasnippet
  :ghook ('prog-mode-hook #'yas-minor-mode)
  :config (yas-reload-all)
  :general (kbd+ "is"
             '(yas-insert-snippet
               :wk "insert snippet")))

(use-package yasnippet-snippets
  :after yasnippet
  :config (yasnippet-snippets-initialize))

(use-package doom-snippets
  :after yasnippet
  :config (doom-snippets-initialize)
  :straight (doom-snippets
             :type git
             :host github
             :repo "hlissner/doom-snippets"))



(kbd+ "ia" #'auto-insert)

(provide 'snippets+)
;;; snippets+.el ends here
