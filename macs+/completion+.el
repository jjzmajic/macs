;;; completion+.el -*- lexical-binding: t; -*-

(setq completion+fallback
      '(company-capf
        company-files
        company-dabbrev-code
        company-dabbrev
        company-ispell))

(after+ 'minibuffer
  (fset 'completion+
        (indirect-function
         'completion-at-point)))

(defun completion+company nil
  (interactive)
  (unless company-mode (company-mode +1))
  (if (minibufferp) (completion+) (helm-company)))

(use-package compdef
  :defer t
  :straight (compdef
             :type git
             :host gitlab
             :repo "jjzmajic/compdef"
             :branch "develop"))

(use-package company
  :init (setq company-idle-delay nil)
  :config (global-company-mode))

(use-package helm-company
  :after helm+
  :config
  (when helm+fuzzy (setq helm-company-fuzzy-match t))
  ;; emulate (setq tab-always-indent 'complete)
  (setq-default tab-always-indent 'complete)
  (after+ 'minibuffer
    (advice+ #'completion-at-point
           :override #'completion+company)))

(provide 'completion+)
;;; company+.el -*- lexical-binding: t; -*-
