;;; nix+.el -*- lexical-binding: t; -*-

(use-package nix-mode
  :defer t)

(provide 'nix+)
;;; nix+.el ends here
